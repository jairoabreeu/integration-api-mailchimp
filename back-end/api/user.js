const mailchimp = require("@mailchimp/mailchimp_marketing");
const oracledb = require('oracledb');
const cron = require('node-cron');
const fs = require('fs');

require('dotenv/config');

mailchimp.setConfig({
    apiKey: process.env.API_KEY,
    server: process.env.SERVER
});

module.exports = app => {

    cron.schedule('0 0 0 * * *', () => {
        emailsDB()
    }, {
        scheduled: true,
        timezone: "America/Manaus"
    })

    const emailsDB = async (req, res) => {

        const rs = await oracledb.getConnection()

        const ret = await rs.execute(`
            select * from (
                    select DISTINCT  
                        pf.nm_pessoa_fisica,  
                        lower(cpf.ds_email) e_mail,
                        pf.ie_sexo,
                        initcap(substr(nm_pessoa_fisica, 1, INSTR(nm_pessoa_fisica,' ', 1) - 1)) as primeiro_nome,
                        initcap(substr(nm_pessoa_fisica, INSTR(nm_pessoa_fisica,' ', 1) + 1, length(nm_pessoa_fisica)   )  ) as sobrenome,
                        pf.dt_nascimento,
                        to_char(pf.dt_atualizacao, 'DD/MM/YYYY HH24:MI:SS') as DT_ATUALIZACAO,
                        tasy.obter_idade(pf.dt_nascimento, sysdate, 'A') as idade     
                    from tasy.pessoa_fisica pf
                    join tasy.compl_pessoa_fisica cpf  on (pf.cd_pessoa_fisica = cpf.cd_pessoa_fisica)
                    where  1=1 
                    and cpf.dt_atualizacao > sysdate - 1 ) a 
            where 1=1 
                and  (  
                    REGEXP_LIKE(upper(a.e_mail), '(COM|COM.BR)$', 'i')
                    or REGEXP_LIKE(upper(a.e_mail), '(ORG|ORG.BR)$', 'i')
                    or REGEXP_LIKE(upper(a.e_mail), '(GOV|GOV.BR)$', 'i')
                    or REGEXP_LIKE(upper(a.e_mail), '(ADV|ADV.BR)$', 'i')
                )
                and REGEXP_LIKE(upper(a.e_mail), '@', 'i')
                and not REGEXP_LIKE(upper(a.e_mail), '^.@', 'i')
                and not REGEXP_LIKE(upper(a.e_mail), '^..@', 'i')
                and  lower(trim(a.e_mail)) not in ( 'samel@samel.com.br', 'samel@samel.com' ) 
        
        `, {}, {outFormat: oracledb.OUT_FORMAT_OBJECT})
            .then(item => {
                return item.rows
            })
            .finally(() => rs.close())
            .catch(err => {
                console.log(err)
            })

            await uploadSave(ret)

            return res.send(ret)

    }

    const uploadSave = async (array, req, res) => {

        for(let x = 1; x < array.length; x++) {
            if(array[x].IE_SEXO == 'M' && array[x].IDADE >= 60){
                await mailchimp.lists.addListMember(process.env.LIST, {
                    email_address: array[x].E_MAIL,
                    status: "subscribed",
                        merge_fields: {
                            FNAME: array[x].PRIMEIRO_NOME,
                            LNAME: array[x].SOBRENOME,
                        },
                        tags: ["Idoso 60+", "Masculino"],      
                })
                .then(() => { fs.appendFile('./src/logs/message.txt', `O e-mail ${array[x].E_MAIL} foi cadastrado com sucesso!\n`, {flag: "a"}, function (err) {
                    if (err) throw err;
                    console.log('Saved!');
                  }) 
                })
                .catch(err => { if(err.status === 400) {
                    console.log('Já cadastrado')
                    } 
                })
            } else if(array[x].IE_SEXO == 'M' && array[x].IDADE >= 40){
                await mailchimp.lists.addListMember(process.env.LIST, {
                    email_address: array[x].E_MAIL,
                    status: "subscribed",
                        merge_fields: {
                            FNAME: array[x].PRIMEIRO_NOME,
                            LNAME: array[x].SOBRENOME,
                        },
                        tags: ["Homem 40+", "Masculino"],      
                })
                .then(() => { fs.appendFile('./src/logs/message.txt', `O e-mail ${array[x].E_MAIL} foi cadastrado com sucesso!\n`, {flag: "a"}, function (err) {
                    if (err) throw err;
                    console.log('Saved!');
                  }) 
                })
                .catch(err => { if(err.status === 400) {
                    console.log('Já cadastrado')
                    } 
                })
            } else if(array[x].IE_SEXO == 'F' && array[x].IDADE >= 60){
                await mailchimp.lists.addListMember(process.env.LIST, {
                    email_address: array[x].E_MAIL,
                    status: "subscribed",
                        merge_fields: {
                            FNAME: array[x].PRIMEIRO_NOME,
                            LNAME: array[x].SOBRENOME,
                        },
                        tags: ["Idoso 60+", "Feminino"],      
                })
                .then(() => { fs.appendFile('./src/logs/message.txt', `O e-mail ${array[x].E_MAIL} foi cadastrado com sucesso!\n`, {flag: "a"}, function (err) {
                    if (err) throw err;
                    console.log('Saved!');
                  }) 
                })
                .catch(err => { if(err.status === 400) {
                    console.log('Já cadastrado')
                    } 
                })
            } else if(array[x].IE_SEXO == 'F' && array[x].IDADE < 60){
                await mailchimp.lists.addListMember(process.env.LIST, {
                    email_address: array[x].E_MAIL,
                    status: "subscribed",
                        merge_fields: {
                            FNAME: array[x].PRIMEIRO_NOME,
                            LNAME: array[x].SOBRENOME,
                        },
                        tags: ["Feminino"],      
                })
                .then(() => { fs.appendFile('./src/logs/message.txt', `O e-mail ${array[x].E_MAIL} foi cadastrado com sucesso!\n`, {flag: "a"}, function (err) {
                    if (err) throw err;
                    console.log('Saved!');
                  }) 
                })
                .catch(err => { if(err.status === 400) {
                    console.log('Já cadastrado')
                    } 
                })
            } else if(array[x].IE_SEXO == 'M' && array[x].IDADE < 40){
                await mailchimp.lists.addListMember(process.env.LIST, {
                    email_address: array[x].E_MAIL,
                    status: "subscribed",
                        merge_fields: {
                            FNAME: array[x].PRIMEIRO_NOME,
                            LNAME: array[x].SOBRENOME,
                        },
                        tags: ["Masculino"],      
                })
                .then(() => { fs.appendFile('./src/logs/message.txt', `O e-mail ${array[x].E_MAIL} foi cadastrado com sucesso!\n`, {flag: "a"}, function (err) {
                    if (err) throw err;
                    console.log('Saved!');
                  }) 
                })
                .catch(err => { if(err.status === 400) {
                        console.log('Já cadastrado')
                    } 
                })
            }
        }

        return console.log('Finalizou importação')
    }

    return { emailsDB, uploadSave }
}
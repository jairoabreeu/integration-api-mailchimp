const mailchimp = require("@mailchimp/mailchimp_marketing");
require('dotenv/config');

mailchimp.setConfig({
    apiKey: process.env.API_KEY,
    server: process.env.SERVER
});

module.exports = app => {
    
    const tag = async (req, res) => {
        const response = await mailchimp.lists.tagSearch(process.env.LIST);
        res.status(200).json(response)
    };

    return { tag }
}
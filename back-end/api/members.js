const mailchimp = require("@mailchimp/mailchimp_marketing");
require('dotenv/config');

mailchimp.setConfig({
    apiKey: process.env.API_KEY,
    server: process.env.SERVER
});

module.exports = app => {

    const { existsOrError, isEmailValid } = app.api.validator
    
    const save = async (req, res) => {

        let values = { fname, lname, email, phone, tag } = req.body

        console.log(values)

        try {
            existsOrError(values.fname, 'Nome não informado')
            existsOrError(values.lname, 'Sobrenome não informado')
            existsOrError(values.email, 'Email não informado')
            isEmailValid(values.email, 'Email não é válido')
            existsOrError(values.phone, 'Telefone não infomado')
            existsOrError(values.tag, 'Nenhuma tag selecionada')

        } catch (msg) {
            return res.status(400).send(msg)
        }

        await mailchimp.lists.addListMember(process.env.LIST, {
        email_address: email,
        status: "subscribed",
            merge_fields: {
                FNAME: fname,
                LNAME: lname,
                PHONE: phone
            },
            tags: tag,      
        })
        .then(() => {res.status(204).send() && console.log('Cadastrado')})
        .catch(erros => res.status(400).json(`E-mail ${email} já cadastrado`))

    }

    const members = async (req, res) => {
        const response = await mailchimp.lists.getListMembersInfo(process.env.LIST, {
            count: 1000,
            sort_field: 'last_changed',
        });
        res.status(200).json(response)
    };

    return { save, members }
}
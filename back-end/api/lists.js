const mailchimp = require("@mailchimp/mailchimp_marketing");
require('dotenv/config');

mailchimp.setConfig({
    apiKey: process.env.API_KEY,
    server: process.env.SERVER
});

module.exports = app => {
    
    const allLists = async (req, res) => {
        const response = await mailchimp.lists.getAllLists();
        res.status(200).json(response)
    }

    const campaigns = async (req, res) => {
        const response = await mailchimp.campaigns.list();
        res.status(200).json(response)
    };

    return { allLists, campaigns }
}
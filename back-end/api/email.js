const mailer = require('nodemailer');
const smtp = require('nodemailer-smtp-transport')
const cron = require('node-cron');
const dotenv = require('dotenv')
const fs = require('fs')

dotenv.config()

module.exports = app => {

    const user = process.env.MANDRILL_USER
    const pass = process.env.MANDRILL_PASS
    const listEmails = ['"Hugo" hugo.lima@samel.com.br ,"Jairo" jairoabreeu@gmail.com']

    cron.schedule('0 30 01 * * *', () => {
        email()
        setTimeout(() => {
            remover()
        }, 20000)
    }, {
        scheduled: true,
        timezone: "America/Manaus"
    })

    const remover = (req, res) => {fs.stat('./src/logs/message.txt', function(err, stats) {
            console.log(stats)

            if(err) {
                return console.log("Arquivo não existe!")
            }

            fs.unlink('./src/logs/message.txt', function(err) {
                if(err) return console.log(err);
                console.log('Arquvo excluído com sucesso!')
            })
        })
    }  

    const email = (req, res) => {

        const transporter = mailer.createTransport(smtp({
            host: process.env.MANDRILL_HOST,
            secure: false,
            port: 587,
            auth: { user, pass },
            tls: {
                // do not fail on invalid certs
                rejectUnauthorized: false
            }
        }))

        transporter.sendMail({
            from: '"Relatório Diário" jairo.abreu@samel.com.br',
            to: listEmails,
            subject: "Notificação relatório diário",
            html: {path: './src/template/index.html'},
            attachments: [
                {
                    filename: 'relatorio.txt',
                    path: './src/logs/message.txt'
                }
            ]
        })
        .then(() => {
            console.log('Enviaado')
        })
        .catch(error => {
            console.log(error)
        })
    }

    return { email }

}
module.exports = app => {
    function existsOrError(value, msg) {
        if (!value) throw msg //Se value for falso, retorna uma mensagem
        if (Array.isArray(value) && value.length == 0) throw msg //Se o array estiver vazio, retorna mensagem
        if (typeof value === 'string' && !value.trim()) throw msg // string em branco, retorna mensagem
    }

    function notExistsOrError(value, msg) {
        try {
            existsOrError(value, msg)
        } catch (msg) {
            return
        }
        throw msg
    }

    function equalsOrError(valueA, valueB, msg) {
        if (valueA !== valueB) throw msg
    }

    function isEmailValid(value, msg) {
        var re = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/

        var valid = re.test(value)

        if (!valid) throw msg

    }

    return { existsOrError, isEmailValid, notExistsOrError, equalsOrError }
}
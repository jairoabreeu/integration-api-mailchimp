module.exports = app => {
    app.route('/users')
        .get(app.api.lists.allLists)
    app.route('/members')
        .get(app.api.members.members)
        .post(app.api.members.save)
    app.route('/tags')
        .get(app.api.tags.tag)
    app.route('/campaigns')
        .get(app.api.lists.campaigns)
    app.route('/dbmails')
        .get(app.api.user.emailsDB)
}
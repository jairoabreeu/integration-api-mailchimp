const oracledb = require('oracledb')
try {
  oracledb.initOracleClient({libDir: '/opt/oracle/instantclient_21_1/'});
} catch (err) {
  console.error('Whoops!');
  console.error(err);
  process.exit(1);
}
const dotenv = require('dotenv')

dotenv.config()

// it's Conneting on Tasy - Oracle 
const connextionConfig = {
    user: process.env.APPV2_USER,
    password: process.env.APPV2_PASSWD,
    connectionString: `${process.env.APPV2_SERVER}/${process.env.APPV2_DB}`,
    poolMin: 5,
    poolMax: 30,
    poolPingInterval: 60,
    autoCommit: true,
    poolTimeout: 360,
    queueTimeout: 2020000,
    batchSize: 50000,
    maxRows:  50000,
}
//oracledb.queueTimeout = 2020000;
const poolPromiseOracle = oracledb.createPool(connextionConfig)
  .then(function (pool){
    console.log('Connected to Oracle')
    return pool
  })
  .catch(function (err){
    console.log('Oracle connection', err)
    return 
  })

module.exports = {
  poolPromiseOracle
}
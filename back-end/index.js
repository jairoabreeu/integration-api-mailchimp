const app = require('express')()
const consign = require('consign')

consign()
    .then('./config/middlewares.js')
    .then('./api/validator.js')
    .then('./config/connection.js')
    .then('./api/user.js')
    .then('./api/lists.js')
    .then('./api/members.js')
    .then('./api/tags.js')
    .then('./api/email.js')
    .then('./config/routes.js')
    .into(app)

app.listen(5000, () => {
    console.log('Aplicação rodando na porta \x1b[32m5000\x1b[0m ...')
})
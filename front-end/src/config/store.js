import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        isMenuVisible: false,
        user: {
            name: 'Hospital Samel',
            email: 'samel@samel.com.br',
            img: 'https://healthtech.samel.com.br/wp-content/themes/healthtech/img/logo.png'
        }
    },
    mutations: {
        toggleMenu(state, isVisible){
            if(isVisible === undefined) {
                state.isMenuVisible = !state.isMenuVisible
            } else {
                state.isMenuVisible = isVisible
            }
        }
    }
})
import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/components/home/Home.vue'
import CadastroEmails from '@/components/register/CadastroEmails.vue'
import NotFound from '@/components/template/NotFound.vue'

Vue.use(VueRouter)

const routes = [{
    name: 'home',
    path: '/',
    component: Home
}, {
    name: 'registerEmails',
    path: '/cadastro',
    component: CadastroEmails
},
{
    path: '/:pathMatch(.*)*',
    //redirect: "/",
    name: 'NotFound',
    meta: { title: 'Erro 404' },
    component: NotFound
}]

export default new VueRouter({
    mode: 'history',
    routes
})

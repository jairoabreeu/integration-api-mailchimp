import Vue from 'vue'
import Toasted from 'vue-toasted'

Vue.use(Toasted, {
    iconPack: 'fontawesome',
    duration: 3000
})

Vue.toasted.register(
    'defaultSuccess',
    payload => !payload.msg ? 'Cadastrado com sucesso' : payload.msg,
    { type: 'success', icon: 'check' }
)

Vue.toasted.register(
    'defaultError',
    payload => !payload.msg ? 'Oops... Erro inesperado.' : payload.msg,
    { type : 'error', icon : 'times' }
)

Vue.toasted.register(
    'update', 
    payload => !payload.msg ? 'Inciando atualização' : 'Inciando atualização',
    { type : 'success', icon : 'fa-paper-plane' }
)